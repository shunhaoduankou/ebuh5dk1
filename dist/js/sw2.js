self.addEventListener('install', function(e) {
    console.log('install success')
});

self.addEventListener('fetch', function(e) {
    console.log(e.request.url);
    // e.respondWith(
    //   caches.match(e.request).then(function(response) {
    //     return response || fetch(e.request);
    //   })
    // );
});
  
self.addEventListener('push', event => {
    const data = event.data.json();
    self.registration.showNotification(data.title, {
        body: data.body,
        icon: 'appIcon/192.png'
    });
})