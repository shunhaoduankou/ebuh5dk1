import{u as e,B as t,E as a,i as l,t as n,a as i,F as o,G as s,H as u,q as c,m as d,I as r,M as m,_ as p}from"./index.e360df02.js";import{m as v,n as h,o as f,p as x}from"./userActions.fab00ef5.js";import{b as y,c as _}from"./BitsActions.3c9882fe.js";import{r as g,s as k,h as w,D as b,aA as C,w as $,j as Q,i as A,T as B,n as N,v as I,M as T,z as U,K as E,F as V,x as M,A as P,aI as j,aD as L,G as R,aE as H,aF as q,q as F,aM as z,t as D,y as G,E as S,aJ as X,aN as J}from"./vendor.0760a54b.js";var O={name:"BitWithdraw",setup(){const{hotData:p}=e(),I=Q(),T=A();let U=g(T.query.bitname||t);const E=a()[U.value+""],V=l(),{XNBBankList:M}=k(V);let P=g(E[0]||"");const j=w((()=>V.certification)),L=()=>{V.reqBindBankList().then((()=>{M.value.length||b.confirm({title:i("提示"),message:i("请先设置您的提币地址"),confirmButtonText:i("设置"),cancelButtonText:i("取消")}).then((()=>{I.replace("/virtual")})).catch((()=>{I.back()}))}))};["themeQIQUAN8","themeQIQUAN9","themeQIQUAN21","themeQIQUAN23"].includes(n().value)?L():v().then((e=>{2!==e?b.confirm({title:i("提示"),message:i(3===e?"身份认证审核中，请耐心等待":"请完成身份认证"),confirmButtonText:i("去认证"),cancelButtonText:i("取消")}).then((()=>{I.replace("/certification")})).catch((()=>{["themeQIQUAN23"].includes(n().value)?L():I.back()})):L()}));let R=g(0);const H=w((()=>{var e,t,a,l;let i=null!=(t=null==(e=M.value)?void 0:e.filter((e=>e.usdt_type===P.value)))?t:[];if(["themeQIQUAN23"].includes(n().value)&&!i.length)for(let n=1;n<E.length;n++){let e=E[n];if(i=null!=(l=null==(a=M.value)?void 0:a.filter((t=>t.usdt_type===e)))?l:[],i.length){P.value=e;break}}return i})),q=w((()=>["themeQIQUAN12","themeQIQUAN23"].includes(n().value)?H.value[R.value]||{}:M.value[R.value]||{}));let F=g(!1);h({pwd_type:1}).then((e=>{F.value=e.count>0}));let z=1;["themeQIQUAN2","themeQIQUAN20"].includes(n().value)&&(z=4),["themeQIQUAN22"].includes(n().value)&&(z=3);const D=o([z]);D[0].name=i(D[0].name);const G=C({minAmount:0,maxAmount:0,count:0,totalAmount:0,withdraw_prompt_word:""});f().then((e=>{G.count=e.withdraw_count,G.totalAmount=e.withdraw_total_amount,G.minAmount=(e.withdraw_min_amount||e.withdraw_min_amount_ali)/1e3,G.maxAmount=(e.withdraw_amount||e.withdraw_amount_ali)/1e3,G.withdraw_prompt_word=e.withdraw_prompt_word}));let S=g(0),X=g(1);y({balance_type_id:z,currency_name:U.value}).then((e=>{S.value=e[U.value+""]||0,X.value=e.state||1}));let J=g(!1);let O=g(""),K=g("");$(O,((e,t)=>{if(e){/^\d+(\.\d{0,3})?$/.test(e)||N((()=>{O.value=t}))}}));let W=g(!1);let Y=g("0"),Z=w((()=>{const{tenantInfo:e}=p,t=JSON.parse(e.value.listFeel||"[]");if(t.unshift({amount:0}),t.push({amount:1e16,withdrawal_fee:t[t.length-1].withdrawal_fee,type:t[t.length-1].type}),!(Number(O.value)>0))return Y.value="0",O.value||"0";for(let a=0;a<t.length;a++){const e=t[a],l=t[a+1];if(Number(O.value)>Number(e.amount)&&Number(O.value)<=Number(l.amount)){if(1===Number(l.type))return Y.value=l.withdrawal_fee+"",(+O.value-l.withdrawal_fee).toFixed(3);if(2===Number(l.type))return Y.value=l.withdrawal_fee+"%",(+O.value-+O.value*l.withdrawal_fee/100).toFixed(3)}}return Y.value="0","0"})),ee=g(!0),te=w((()=>ee.value?"password":"text"));const ae=g("获取验证码");let le=null;const ne=g(!1),ie=g(""),oe=g(""),se=g(!1),ue=w((()=>{var e,t;return["themeQIQUAN23"].includes(n().value)?!(!["1","2"].includes(null==(e=j.value)?void 0:e.check)||oe.value)||(!(!["1"].includes(null==(t=j.value)?void 0:t.check)||ie.value)||(!O.value||!q.value.pay_no)):!O.value||!q.value.pay_no||!K.value}));return{emailLoading:se,withdrawBtnDisable:ue,emailCodeTxt:ae,emailCodeSending:ne,getEmailCode:async()=>{clearInterval(le),se.value=!0;try{const e=await V.reqEmailCode({email_to_account:j.value.email,email_type:11});se.value=!1,e&&"9999"===e.response_code?((()=>{let e=60;ne.value=!0,ae.value=`${e}s`,le=setInterval((()=>{e--,ae.value=`${e}s`,e<=0&&(clearInterval(le),ae.value="获取验证码",ne.value=!1)}),1e3)})(),B.success(i("发送成功"))):B.fail(i(e.info))}catch(e){se.value=!1,e&&e.data&&e.data.info?d(e.data.info,2):d(e.info||e.data.info,2)}},emailCode:ie,googleCode:oe,maskEmail:s,displayCardcode:u,certificationInfo:j,account_list:D,bitname:U,withdrawLimt:G,userBalance:S,XNBBankList:M,xieyi_type:E,active_type:P,bankList:H,bankIdx:R,currentBank:q,showBankPicker:J,onConfirmBank:(e,t)=>{R.value=t,J.value=!1},allNum:()=>{let e=S.value+"",t=e.indexOf(".");-1!==t&&(e=e.substring(0,t+4)),O.value=e},isReq:W,hendlePost:async()=>{var e,t,a,l;if(!_(X.value,n().value))return!1;let o=+O.value.trim();if(o<G.minAmount)return c(i("提币数量不能少于")+" "+G.minAmount);if(o>G.maxAmount)return 0===G.maxAmount&&G.withdraw_prompt_word?c(i(G.withdraw_prompt_word)):c(i("提币数量不能大于")+" "+G.maxAmount);if(["themeQIQUAN23"].includes(n().value)){if(["1","2"].includes(null==(e=j.value)?void 0:e.check))try{const e=await V.checkGoogleCode({googleCode:oe.value,check_type:1});if(!e||"9999"!==e.response_code)return d(e,2),!1}catch(u){return u&&u.data&&u.data.info?d(u.data.info,2):d((null==u?void 0:u.info)||(null==(t=null==u?void 0:u.data)?void 0:t.info),2),!1}if(["1"].includes(null==(a=j.value)?void 0:a.check))try{const e=await V.checkEmailCode({email:j.value.email,type:11,emailCode:ie.value});if(!e||"9999"!==e.response_code)return d(e,2),!1}catch(u){return u&&u.data&&u.data.info?d(u.data.info,2):d((null==u?void 0:u.info)||(null==(l=null==u?void 0:u.data)?void 0:l.info),2),!1}}let s={amount:r(o),acct_pwd:K.value.trim(),open_addr:q.value.open_addr,pay_org_id:q.value.pay_org_id,pay_name:q.value.pay_name,pay_org_name:q.value.pay_org_name,pay_no:q.value.pay_no,pay_info_id:q.value.pay_info_id,account_type:D[0].id,currency_name:U.value,currency:"",rate:1};W.value=!0;try{await x(s)&&(m("withdrawClick",{currency:U.value,value:o}),O.value="",K.value="",c(i("提币成功"),1),setTimeout((()=>{I.replace("/mine")}),2e3))}catch(p){if(p){let e=p.info;if(/打码量/.test(e))c(i("尊敬的用户，您的保证金周期未结束，无法申请出售"),2);else{let t=e.replace(/[^0-9]/gi,"");/^单日提现金额最高/.test(e)?c(i("单日提现金额最高:{num}！",{num:t}),2):/^单日最多提现[0-9]+次/.test(e)?c(i("单日最多提现{num}次",{num:t}),2):/^今日可提现额度为/.test(e)?c(i("今日可提现额度为:{num}",{num:t}),2):d(e,2)}}}W.value=!1},amount:O,acct_pwd:K,getRealAmount:Z,nosetpwd:F,eyeTogger:ee,nameType:te,onEyeTogger:()=>ee.value=!ee.value,showFee:Y}}};const K={class:"page-1 bitwithdraw-page"},W={class:"page-main page-margin-top-0"},Y={class:"url-type"},Z={class:"name"},ee={key:1,class:"text-desc fz-13"},te={class:"mx-1"},ae={class:"page-main"},le={class:"url-type d-flex align-items-center justify-content-between"},ne={class:"name"},ie={class:"value d-flex align-items-center justify-content-end"},oe=["textContent"],se=["onClick","textContent"],ue={class:"page-main"},ce={class:"fz-14 text-ellipsis"},de={class:"coust-input-num d-flex align-items-center"},re=["textContent"],me={class:"fz-14 text-ellipsis pt-2"},pe={class:"pwd-box"},ve={class:"d-flex align-items-center justify-content-between"},he=["textContent"],fe={class:"fz-14 text-ellipsis pt-2"},xe={class:"pwd-box"},ye={class:"fz-14 text-ellipsis pt-2"},_e={class:"pwd-box"},ge={class:"fz-14 text-ellipsis pt-2"},ke={class:"pwd-box"},we=(e=>(H("data-v-2d4b2fec"),e=e(),q(),e))((()=>E("div",{class:"pd-ft"},null,-1))),be={class:"page-bottom d-flex align-items-center justify-content-between"},Ce={class:"bottom-left"},$e=["textContent"],Qe=["textContent"],Ae={class:"btn-box"},Be={class:"top d-flex align-items-center"},Ne=["textContent"],Ie={class:"page-main"},Te={class:"url-type d-flex align-items-center justify-content-between"},Ue={class:"name"},Ee={class:"value d-flex align-items-center justify-content-end"},Ve=["textContent"],Me={class:"page-main"},Pe={class:"url-type"},je={class:"name"},Le={key:1,class:"text-desc fz-13"},Re={class:"mx-1"},He={class:"page-main"},qe={class:"coust-input-num d-flex align-items-center"},Fe=["textContent"],ze={class:"pwd-box"},De=["textContent"],Ge=["textContent"],Se={class:"d-flex align-items-center justify-content-between"},Xe=["textContent"],Je={class:"btn-box mt-3"};var Oe=p(O,[["render",function(e,t,a,l,n,i){var o,s,u;const c=F("CustomHeader"),d=F("van-cell"),r=F("van-picker"),m=F("van-popup"),p=F("van-field"),v=F("router-link"),h=F("van-icon"),f=F("van-button"),x=F("CustomPageTitle"),y=z("preventReClick");return D(),I("div",K,[["themeQIQUAN12","themeQIQUAN23"].includes(e.THEME)?(D(),I(T,{key:0},[U(c,{title:e.$t("转账")+" "+e.bitname,rightText:e.$t("记录"),onClickRight:t[0]||(t[0]=t=>e.$router.push("/orderlist?type=1"))},null,8,["title","rightText"]),E("div",W,[E("div",Y,[E("span",Z,V(e.$t("提币地址")),1),e.currentBank.pay_no?(D(),M(d,{key:0,class:"select-bank",title:e.displayCardcode(e.currentBank.pay_no),"is-link":"",onClick:t[1]||(t[1]=t=>e.showBankPicker=!0)},null,8,["title"])):(D(),I("div",ee,[E("span",te,V(e.$t("没有该类型的地址")),1),E("span",{class:"text-grad go-addurl",onClick:t[2]||(t[2]=t=>e.$router.push(`/virtualadd?usdttype=${e.active_type}&bitname=${e.bitname}`))},V(e.$t("创建新地址")),1)])),U(m,{show:e.showBankPicker,"onUpdate:show":t[4]||(t[4]=t=>e.showBankPicker=t),position:"bottom",teleport:"#app-container"},{default:P((()=>[U(r,{columns:e.bankList,"columns-field-names":{text:"pay_no"},"visible-item-count":"6","item-height":"32","default-index":e.bankIdx,onConfirm:e.onConfirmBank,onCancel:t[3]||(t[3]=t=>e.showBankPicker=!1),"cancel-button-text":e.$t("取消"),"confirm-button-text":e.$t("确认")},null,8,["columns","default-index","onConfirm","cancel-button-text","confirm-button-text"])])),_:1},8,["show"])])]),E("div",ae,[E("div",le,[E("span",ne,V(e.$t("协议")),1),E("div",ie,[["themeQIQUAN23"].includes(e.THEME)&&e.currentBank.pay_no?(D(),I("span",{key:0,class:"mx-1 position-relative bitname-item text-grad",textContent:V(e.currentBank.usdt_type)},null,8,oe)):(D(!0),I(T,{key:1},j(e.xieyi_type,(t=>(D(),I("span",{key:t,class:G(["mx-1 position-relative",[e.active_type===t?"bitname-item text-grad":""]]),onClick:a=>e.active_type=t,textContent:V(t)},null,10,se)))),128))])])]),E("div",ue,[E("p",ce,V(e.$t("金额")),1),E("div",de,[U(p,{modelValue:e.amount,"onUpdate:modelValue":t[5]||(t[5]=t=>e.amount=t),maxlength:"10",type:"number",autocomplete:"off",placeholder:e.$t("最低提币")+e.withdrawLimt.minAmount,clearable:""},null,8,["modelValue","placeholder"]),E("span",{class:"pifex",textContent:V(e.bitname)},null,8,re),E("span",{class:"all text-ellipsis",onClick:t[6]||(t[6]=L(((...t)=>e.allNum&&e.allNum(...t)),["stop"]))},V(e.$t("全部")),1)]),E("p",me,V(e.$t("账户类型")),1),E("div",pe,[U(p,{modelValue:e.account_list[0].name,"onUpdate:modelValue":t[7]||(t[7]=t=>e.account_list[0].name=t),readonly:""},null,8,["modelValue"])]),E("div",ve,[E("p",{class:"user-perice",textContent:V(`${e.$t("可用")}:${e.userBalance||0} ${e.bitname}`)},null,8,he),U(v,{class:"mx-2 text-title",to:"/transfer"},{default:P((()=>[S(V(e.$t("划转")),1)])),_:1})]),E("p",fe,V(e.$t("资金密码")),1),E("div",xe,[U(p,{modelValue:e.acct_pwd,"onUpdate:modelValue":t[8]||(t[8]=t=>e.acct_pwd=t),maxlength:"10",type:e.nameType,placeholder:e.$t("请输入资金密码"),clearable:""},{"right-icon":P((()=>[U(h,{name:e.eyeTogger?"closed-eye":"eye-o",onClick:e.onEyeTogger},null,8,["name","onClick"])])),_:1},8,["modelValue","type","placeholder"])]),["themeQIQUAN23"].includes(e.THEME)?(D(),I(T,{key:0},[["1"].includes(null==(o=e.certificationInfo)?void 0:o.check)?(D(),I(T,{key:0},[E("p",ye,V(e.$t("邮箱验证码"))+"(To:"+V(e.maskEmail(null==(s=e.certificationInfo)?void 0:s.email))+")",1),E("div",_e,[U(p,{modelValue:e.emailCode,"onUpdate:modelValue":t[9]||(t[9]=t=>e.emailCode=t),modelModifiers:{trim:!0},maxlength:"10",placeholder:e.$t("请输入邮箱验证码"),clearable:""},{button:P((()=>[X((D(),M(f,{type:"primary",loading:e.emailLoading,disabled:e.isReq,onClick:e.getEmailCode,class:"sms_code",style:J(e.emailCodeSending?"background-color: #d9d9d9;":"")},{default:P((()=>[S(V(e.mb_t(e.emailCodeTxt)),1)])),_:1},8,["loading","disabled","onClick","style"])),[[y]])])),_:1},8,["modelValue","placeholder"])])],64)):R("",!0),["1","2"].includes(null==(u=e.certificationInfo)?void 0:u.check)?(D(),I(T,{key:1},[E("p",ge,V(e.$t("谷歌验证码")),1),E("div",ke,[U(p,{modelValue:e.googleCode,"onUpdate:modelValue":t[10]||(t[10]=t=>e.googleCode=t),modelModifiers:{trim:!0},maxlength:"10",placeholder:e.$t("请输入谷歌验证码"),clearable:""},null,8,["modelValue","placeholder"])])],64)):R("",!0)],64)):R("",!0),we,E("div",be,[E("div",Ce,[E("p",{class:"current-perice",textContent:V(`${e.$t("手续费")}: ${e.showFee}`)},null,8,$e),E("p",{class:"current-perice",textContent:V(`${e.$t("实际到账")}: ${e.getRealAmount} ${e.bitname}`)},null,8,Qe)]),E("div",Ae,[U(f,{class:"btn-success",disabled:e.withdrawBtnDisable,loading:e.isReq,onClick:e.hendlePost},{default:P((()=>[S(V(e.$t("提币")),1)])),_:1},8,["disabled","loading","onClick"])])])])],64)):(D(),I(T,{key:1},[["themeQIQUAN13"].includes(e.THEME)?(D(),M(c,{key:0,title:e.$t("出金"),rightText:e.$t("记录"),onClickRight:t[11]||(t[11]=t=>e.$router.push("/orderlist?type=1"))},null,8,["title","rightText"])):R("",!0),U(c,{title:e.$t("提币"),rightText:e.$t("记录"),onClickRight:t[12]||(t[12]=t=>e.$router.push("/orderlist?type=1"))},null,8,["title","rightText"]),E("div",Be,[U(x),E("span",{class:"fw-bold green",textContent:V(e.bitname)},null,8,Ne)]),E("div",Ie,[E("div",Te,[E("span",Ue,V(e.$t("协议")),1),E("div",Ee,[E("span",{class:"mx-1 position-relative bitname-item",textContent:V(e.currentBank.usdt_type)},null,8,Ve)])])]),E("div",Me,[E("div",Pe,[E("span",je,V(e.$t("提币地址")),1),e.currentBank.pay_no?(D(),M(d,{key:0,class:"select-bank",title:e.currentBank.pay_no,"is-link":"",onClick:t[13]||(t[13]=t=>e.showBankPicker=!0)},null,8,["title"])):(D(),I("div",Le,[E("span",Re,V(e.$t("没有该类型的地址")),1),E("span",{class:"text-grad go-addurl",onClick:t[14]||(t[14]=t=>e.$router.push(`/virtualadd?usdttype=${e.active_type}&bitname=${e.bitname}`))},V(e.$t("创建新地址")),1)])),U(m,{show:e.showBankPicker,"onUpdate:show":t[16]||(t[16]=t=>e.showBankPicker=t),position:"bottom",teleport:"#app-container"},{default:P((()=>[U(r,{columns:e.XNBBankList,"columns-field-names":{text:"pay_no"},"visible-item-count":"6","item-height":"32","default-index":e.bankIdx,onConfirm:e.onConfirmBank,onCancel:t[15]||(t[15]=t=>e.showBankPicker=!1),"cancel-button-text":e.$t("取消"),"confirm-button-text":e.$t("确认")},null,8,["columns","default-index","onConfirm","cancel-button-text","confirm-button-text"])])),_:1},8,["show"])])]),E("div",He,[E("div",qe,[U(p,{modelValue:e.amount,"onUpdate:modelValue":t[17]||(t[17]=t=>e.amount=t),maxlength:"10",type:"number",placeholder:e.$t("最低提币")+e.withdrawLimt.minAmount,clearable:""},null,8,["modelValue","placeholder"]),"themeQIQUAN13"!==e.THEME?(D(),I("span",{key:0,class:"pifex",textContent:V(e.bitname)},null,8,Fe)):R("",!0),E("span",{class:"all text-ellipsis",onClick:t[18]||(t[18]=L(((...t)=>e.allNum&&e.allNum(...t)),["stop"]))},V(e.$t("全部")),1)]),E("div",ze,[U(p,{modelValue:e.acct_pwd,"onUpdate:modelValue":t[19]||(t[19]=t=>e.acct_pwd=t),maxlength:"10",type:e.nameType,placeholder:e.$t("请输入资金密码"),clearable:""},{"right-icon":P((()=>[U(h,{name:e.eyeTogger?"closed-eye":"eye-o",onClick:e.onEyeTogger},null,8,["name","onClick"])])),_:1},8,["modelValue","type","placeholder"])]),E("p",{class:"current-perice",textContent:V(`${e.$t("手续费")}: ${e.showFee}`)},null,8,De),E("p",{class:"current-perice",textContent:V(`${e.$t("实际到账")}: ${e.getRealAmount} ${"themeQIQUAN13"===e.THEME?"":e.bitname}`)},null,8,Ge),E("div",Se,[E("p",{class:"user-perice",textContent:V(`${e.$t("可用")}:${e.userBalance||0} ${"themeQIQUAN13"===e.THEME?"":e.bitname}`)},null,8,Xe),["themeQIQUAN2"].includes(e.THEME)?R("",!0):(D(),M(v,{key:0,class:"mx-2 text-dark",to:"/transfer"},{default:P((()=>[S(V(e.$t("划转")),1)])),_:1}))]),E("div",Je,[U(f,{class:"btn-fail",disabled:!e.amount||!e.currentBank.pay_no||!e.acct_pwd,loading:e.isReq,onClick:e.hendlePost},{default:P((()=>[S(V(e.$t("提币")),1)])),_:1},8,["disabled","loading","onClick"])])])],64))])}],["__scopeId","data-v-2d4b2fec"]]);export{Oe as default};
